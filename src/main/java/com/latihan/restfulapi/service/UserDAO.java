package com.latihan.restfulapi.service;

import com.latihan.restfulapi.model.QueryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class UserDAO {

    @Autowired
    private EntityManager em;

    public List<QueryModel> getQueryNative(){
        String queryScript = "SELECT RAND(100) AS id, u.username, r.role_name, p.url \n" +
                "FROM user u \n" +
                "INNER JOIN role r ON r.id=u.role_id \n" +
                "INNER JOIN permission p ON p.role_id=r.id \n";

        // Pakai objek query nya
        Query query = em.createNativeQuery(queryScript, "QueryNative");

        // Tangkap hasilnya
        List<QueryModel> list = query.getResultList();
        return list;
    }
}
