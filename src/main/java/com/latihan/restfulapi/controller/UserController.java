package com.latihan.restfulapi.controller;

import com.latihan.restfulapi.model.QueryModel;
import com.latihan.restfulapi.model.UserModel;
import com.latihan.restfulapi.service.UserDAO;
import com.latihan.restfulapi.service.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.QualifierEntry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/user")

public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDAO userDAO;


    @GetMapping(path = "/getAllUser")
    public ResponseEntity<List<UserModel>> getAllUser(){
        List<UserModel> u = userRepository.findAll();

        if (u.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(u, HttpStatus.OK);
    }

    @GetMapping(path = "/getUserById")
    public ResponseEntity<UserModel> getUserById(@RequestParam(name = "userId") Integer id){
        UserModel u = userRepository.findById(id).get();

        return new ResponseEntity<>(u, HttpStatus.OK);
    }

    @PostMapping(path = "/addUser")
    public ResponseEntity<String> addUser(@RequestBody UserModel userModel){
        UserModel u = userRepository.save(userModel);

        return new ResponseEntity<>("Saved", HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/deleteUser")
    public ResponseEntity<String> deleteUser(@RequestParam(name = "userId") Integer id){
         try {
             userRepository.deleteById(Integer.valueOf(id));
         }catch (Exception e){
             return new ResponseEntity<>("Failed, Data nout found!", HttpStatus.NOT_FOUND);
         }

         return new ResponseEntity<>("Deleted!", HttpStatus.OK);
    }

    // Custom query
    @GetMapping(path = "/getCustomQuery")
    public ResponseEntity<List<QueryModel>> getCustomQuery(){
        List<QueryModel> list = userDAO.getQueryNative();

        return ResponseEntity.ok(list);
    }

}
