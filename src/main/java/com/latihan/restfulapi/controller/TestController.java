package com.latihan.restfulapi.controller;

import com.latihan.restfulapi.model.TestModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/test")
public class TestController {

    @GetMapping(path = "/contoh")
    public ResponseEntity<TestModel> contoh(){
        TestModel mhs = new TestModel();
        mhs.setNama("Elvin");
        mhs.setAlamat("Indramayu");

        return ResponseEntity.ok(mhs);
    }
}
