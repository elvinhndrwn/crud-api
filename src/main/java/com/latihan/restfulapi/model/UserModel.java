package com.latihan.restfulapi.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user")

public class UserModel {

    @Id
    private Integer id;
    private String roleId;
    private String username;
    private String email;
    private String address;

    @OneToOne(optional = false)
    @JoinColumn(name = "roleId", insertable = false, updatable = false)
    private RoleModel roleModel;
}

