package com.latihan.restfulapi.model;

import jdk.jfr.Enabled;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "role")

public class RoleModel {

    @Id
    private String id;
    private String roleName;
    private String description;

    // Karena di Class Permission sudah di relasikan maka di Role harus di mapped kan:
    @OneToMany(mappedBy = "roleModel")
    private List<PermissionModel> permissionModelList;
}
