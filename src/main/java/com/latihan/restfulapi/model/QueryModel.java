package com.latihan.restfulapi.model;

import jdk.jfr.Enabled;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "QueryNative", entities = {
        @EntityResult(entityClass = QueryModel.class, fields = {
                @FieldResult(name = "id", column = "id"),
                @FieldResult(name = "username", column = "username"),
                @FieldResult(name = "roleName", column = "role_name"),
                @FieldResult(name = "url", column = "url"),
        })
})
public class QueryModel {

    // Setiap query harus mempunyai 1 field yg memiliki nilai UNIK

    @Id
    private String id;
    private String username;
    private String roleName;
    private String url;
}
