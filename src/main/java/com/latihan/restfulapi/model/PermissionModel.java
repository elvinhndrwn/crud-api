package com.latihan.restfulapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jdk.jfr.Enabled;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "permission")

public class PermissionModel {

    @Id
    private String id;
    private String modul;
    private String url;

    // Karena field role_id ada relasi ke tabel role maka :
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")

    //json backreference hanya khusus di sisi child nya, many to one
    @JsonBackReference

    private RoleModel roleModel;
}
